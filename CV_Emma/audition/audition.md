#### 关于script标签
async：布尔值，可以实现异步加载，即文档解析的同时可以执行异步脚本
defer：布尔值，通知浏览器该脚本将在文档完成解析后，触发 DOMContentLoaded 事件前执行，可以执行DOM操作
async和defer共同点：必须有src属性，对内联脚本无作用
什么都不写默认情况下，一般都将script标签写在HTML的底部，因为这个时候DOM树已经加载完成了

