import React, { Component } from 'react'

class Calculagraph extends Component {
  constructor(props) {
    super(props)
    this.state = {
      hours: "00",
      minutes: "00",
      seconds: "00",
      // timeId: setInterval(this._setTime(), 1000)
    }
    // this.timeId = setInterval(this._setTime(), 1000)
  }
  componentDidMount() {
    this.mySetTime.call(this)
  }
  render() {
    return (
      <div>
        <span>{this.state.hours}</span>
        <span>:</span>
        <span>{this.state.minutes}</span>
        <span>:</span>
        <span>{this.state.seconds}</span>
      </div>
    )
  }
  // _setTime() {
  //   var nowTime = new Date()
  //   var laterTime = new Date(2018, 4, 30, 18, 30, 0)
  //   var time = parseInt((laterTime - nowTime) / 1000, 10)
  //   if (time < 0) {
  //     time = 0
  //     clearInterval(this.state.timeId)
  //   }
  //   var hours = parseInt(time / 3600, 10)
  //   var minutes = parseInt(time / 60, 10) % 60
  //   var seconds = time % 60

  // }
  mySetTime() {

    //思路：
    //1. 有一个当前的时候，还需要一个秒杀的时间。 获得时间差
    //2. 把时间差转换成 小时  分钟 秒钟
    //3. 设置到span中
    //4. 开启一个定时器，每秒终都执行。
    //5. 当秒杀时间到了，应该清除定时器。

    function setTime() {
      //data:数据  date:日期
      var nowTime = new Date();

      //创建时间不推荐使用字符串。
      //月份是从0开始的
      var secTime = new Date(2018, 4, 30, 23, 23, 0);

      //时间差, 获取的秒数
      var time = parseInt((secTime - nowTime) / 1000);

      if (time <= 0) {
        time = 0;
        clearInterval(timeId);
      }

      //转换成小时部分  1小时 = 3600
      var hours = parseInt(time / 3600);
      //console.log(hours);

      //转换成分钟  1小时=60  获取到不足60的分钟
      var minutes = parseInt(time / 60) % 60;
      //console.log(minutes);

      //转换成秒钟
      var seconds = time % 60;
      //console.log(seconds);

      //把时间显示到span上
      this.setState({ hours: addZero(hours) })
      this.setState({ minutes: addZero(minutes) })
      this.setState({ seconds: addZero(seconds) })
    }

    function addZero(n) {
      return n < 10 ? "0" + n : n;
    }
    //定时器
    var thisSetTime = setTime.bind(this);
    thisSetTime()
    var timeId = setInterval(thisSetTime, 1000);
  }
}
export default Calculagraph
